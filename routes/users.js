const errors = require('restify-errors');
const User = require('../models/User');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const auth = require('../auth');
const config = require('../config');

module.exports = server => {
    // Register User
    server.post('/register',  async (req, res, next) => {
        if(!req.is('application/json')) {
            return next(new errors.InvalidContentError("Expected content type: 'application/json'"))
        }

        const {email, password} = req.body;
        bcrypt.genSalt(10, (error, salt) =>{
            bcrypt.hash(password, salt, async (error, hash) => {
                const user = new User({email, password: hash});
                try {
                    const newuser = await user.save();
                    res.send(201);
                    next(); 
                } catch (error) {
                    return next(new errors.InternalError(error.message));
                }
            });
        });
    });

    // Auth User
    server.post('/auth', async (req, res, next) => {
        const {email, password} = req.body;
        try {
            const user = await auth.authenticate(email, password);
            
            const token = jwt.sign(user.toJSON(), config.JWT_SECRET, { expiresIn: '1h'});

            const {iat, exp } = jwt.decode(token);
            res.send({iat, exp, token});

            next();
        } catch (error) {
            return next(new errors.UnauthorizedError (error));
        }
    });
}