const errors = require('restify-errors');
const Customer = require('../models/Customer');

module.exports = server => {
    // Get Customers
    server.get('/customers',  async (req, res, next) => {
        try {
            const customers = await Customer.find({});
            res.send(customers);
            next();
        } catch (error) {
            return next(new errors.InvalidContentError(error));
        }
    });

    // Get Customer
    server.get('/customers/:id',  async (req, res, next) => {
        try {
            const customer = await Customer.findById(req.params.id);
            res.send(customer);
            next();
        } catch (error) {
            return next(new errors.ResourceNotFoundError(`No customer with id ${req.params.id}`));
        }
    });

    // Add customer
    server.post('/customers',  async (req, res, next) => {
        if(!req.is('application/json')) {
            return next(new errors.InvalidContentError("Expected content type: 'application/json'"))
        }

        const {name, email, balance} = req.body;
        const customer = new Customer({name, email, balance});

        try {
            const newCustomer = await customer.save();
            res.send(201);
            next();
        } catch (error) {
            return next(new errors.InternalError(error.message));
        }
    });

    // Update customer
    server.put('/customers/:id',  async (req, res, next) => {
        if(!req.is('application/json')) {
            return next(new errors.InvalidContentError("Expected content type: 'application/json'"))
        }

        try {
            const customer = await Customer.findByIdAndUpdate(req.params.id, req.body);
            res.send(200);
            next();
        } catch (error) {
            return next(new errors.ResourceNotFoundError(`No customer with id ${req.params.id}`));
        }
    });

    // Delete Customer
    server.del('/customers/:id',  async (req, res, next) => {
        try {
            const customer = await Customer.findByIdAndRemove(req.params.id);
            res.send(204);
            next();
        } catch (error) {
            return next(new errors.ResourceNotFoundError(`No customer with id ${req.params.id}`));
        }
    });
}